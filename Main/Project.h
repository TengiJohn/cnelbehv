#pragma once
#include "AeDynArray.h"
#include "DataGlove.h"
class Project
{
public:
	__declspec(dllexport) Project(void);
	__declspec(dllexport) ~Project(void);

	AeDynArray<DataGlove> dgarray;
	int numgloves;
	int GetNewId(void);
	int CloseAllDataGloveThread(void);
	int EmptyDataGlove(void);
	__declspec(dllexport) bool RemoveGlove(int gloveid);
	__declspec(dllexport) int AddDataGlove(const char * label, int comport, int type);
	__declspec(dllexport) DataGlove *GetDataGlove(int id);
	__declspec(dllexport) DataGlove *GetDataGloveByPos(int pos);
	__declspec(dllexport) int SaveProject(void);
	__declspec(dllexport) int LoadProject(void);
	__declspec(dllexport) int GetNumDataGlove(void);
	__declspec(dllexport) void CloseProject(void);

	__declspec(dllexport) int StartSampling(int gloveid);
	__declspec(dllexport) int StopSampling(int gloveid);
	int SetGloveComPort(int gloveid, int comport);
	int FreeResource(void);
	__declspec(dllexport) int SetGloveBufferSize(int size_finger, int size_acc, int gloveid);
};
