#include "Sensor.h"

Sensor::Sensor() {
	max_sensor = MAX_SENSORS;
	TryTime = TIME_CONNECT * 1000;

	project = new Project();

	datagloves = NULL;

	sensor_id = new int[max_sensor];

	sensor_number = 0;

	//We only have 2 VHand3.0 data glove
	datagloves = new VHand30[4];

	teensy_connected = false;
}

Sensor:: ~Sensor()
{
	delete project;
	delete[] sensor_id;
	delete[] datagloves;
}

bool Sensor::isTeensyConnected()
{
	return teensy_connected;
}
bool Sensor::isGloveConnected(int id)
{
	switch (id)
	{
	case ID_VHAND_30_LEFT:
		return (datagloves[ID_VHAND_30_LEFT].GetConnectionStatus() == WIFI_CONNECTED);
		break;
	case ID_VHAND_30_RIGHT:
		return (datagloves[ID_VHAND_30_RIGHT].GetConnectionStatus() == WIFI_CONNECTED);
		break;
	case ID_VHAND_30_LEFT_USB:
		return (datagloves[ID_VHAND_30_LEFT_USB].GetConnectionStatus() == USB_CONNECTED);
		break;
	case ID_VHAND_30_RIGHT_USB:
		return (datagloves[ID_VHAND_30_RIGHT_USB].GetConnectionStatus() == USB_CONNECTED);
		break;
	case ID_VHAND_20_LEFT:
		if (!project->GetNumDataGlove())
			return false;
		else
		{
			if (isSensorAdded(ID_VHAND_20_LEFT))
				return true;
			else
				return false;
		}
		break;
	case ID_VHAND_20_RIGHT:

		if (!project->GetNumDataGlove())
			return false;
		else
		{
			if (isSensorAdded(ID_VHAND_20_RIGHT))
				return true;
			else
				return false;
		}
	default:
		return false;
	}
}
bool Sensor::isSensorAdded(int id)
{
	for (int i = 0; i < sensor_number; i++)
	{
		if (id==sensor_id[i])
			return true;
	}
	return false;
}

int Sensor::ConnectVHand30(VHand30* dg,int CONN_TYPE,int STREAM_MODE, bool silent)
{

	dg->Connect(CONN_TYPE, STREAM_MODE);
	long start = ::GetTickCount();

	while (dg->Connected == NOT_CONNECTED)
	{
		if (GetTickCount() - start>TryTime)
		{
			dg->Disconnect();

			if (!silent)
			MessageBox(NULL, L"Cannot connect the data glove !", L"Error", MB_OK | MB_ICONERROR);
			return GLOVE_ADD_FAIL;
		}
	}

	if (((dg->GetConnectionStatus() == USB_CONNECTED) && (CONN_TYPE == CONN_USB)) ||
		((dg->GetConnectionStatus() == WIFI_CONNECTED) && (CONN_TYPE == CONN_WIFI)))
	{
		if (!silent)
			MessageBox(NULL, L"Add data glove success, start sampling !", L"Success", MB_OK | MB_ICONINFORMATION);
		return GLOVE_ADD_SUCCESS;
	}
	else
	{
		if (!silent)
			MessageBox(NULL, L"Cannot connect the data glove !", L"Error", MB_OK | MB_ICONERROR);
		return GLOVE_ADD_FAIL;
	}
}

//If successfully found, will return the COM port number, else GLOVE_ADD_FAIL
int Sensor::VHand30_ScanCOM(VHand30* dg,char* ip)
{
	int connect_status=GLOVE_ADD_FAIL;
	for (int i = 1; i <= 50; i++)
	{
		dg->SetConnectionParameters(i, ip);
		connect_status = ConnectVHand30(dg, CONN_USB, STREAM_FINGERS_QUATERNION, true);

		if (connect_status == GLOVE_ADD_SUCCESS && dg->GetConnectionStatus()==USB_CONNECTED)
		{
			TCHAR szBuf[512];
			HRESULT hr = StringCchPrintf(szBuf, 512, TEXT("Auto discovered at COM%d, start sampling!"), i);
			MessageBox(NULL, szBuf, L"Success", MB_OK | MB_ICONINFORMATION);

			return i;
		}
	}

		MessageBox(NULL, L"Cannot find the device from COM1 to COM50 !", L"Error", MB_OK | MB_ICONERROR);
		return GLOVE_ADD_FAIL;


}
int Sensor::addGlove(int id) {
	// put 0 as side; to check.
	int connect_status;
	switch (id)
	{
	case ID_VHAND_30_LEFT:
		if (isSensorAdded(ID_VHAND_30_LEFT))
		{
			MessageBox(NULL, L"Data glove already added !", L"Warning", MB_OK | MB_ICONWARNING);
			return 0;
		}

		//reserved in the router
		datagloves[ID_VHAND_30_LEFT].SetConnectionParameters(VHand3_L_Com, IP_VHAND3_L);

		connect_status = ConnectVHand30(&datagloves[ID_VHAND_30_LEFT], CONN_WIFI, STREAM_FINGERS_QUATERNION, false);

		if (connect_status == GLOVE_ADD_SUCCESS)
		{
			sensor_id[sensor_number] = ID_VHAND_30_LEFT;
			sensor_number++;
		}
		return connect_status;
		break;
	case ID_VHAND_30_RIGHT:
		if (isSensorAdded(ID_VHAND_30_RIGHT))
		{
			MessageBox(NULL, L"Data glove already added !", L"Warning", MB_OK | MB_ICONWARNING);
			return 0;
		}

		datagloves[ID_VHAND_30_RIGHT].SetConnectionParameters(VHand3_R_Com, IP_VHAND3_R);

		connect_status = ConnectVHand30(&datagloves[ID_VHAND_30_RIGHT], CONN_WIFI, STREAM_FINGERS_QUATERNION, false);

		if (connect_status == GLOVE_ADD_SUCCESS)
		{
			sensor_id[sensor_number] = ID_VHAND_30_RIGHT;
			sensor_number++;
		}
		return connect_status;
		break;
	case ID_VHAND_30_LEFT_USB:

		if (VHand3_L_Com == 0)
		{
			//Auto scan the port from 1-50
			connect_status = VHand30_ScanCOM(&datagloves[ID_VHAND_30_LEFT_USB],IP_VHAND3_L);

			if (connect_status!= GLOVE_ADD_FAIL)
			{
				VHand3_L_Com = connect_status;
			}
		}
		else
		{
			if (isSensorAdded(ID_VHAND_30_LEFT_USB))
			{
				MessageBox(NULL, L"Data glove already added !", L"Warning", MB_OK | MB_ICONWARNING);
				return 0;
			}

			datagloves[ID_VHAND_30_LEFT_USB].SetConnectionParameters(VHand3_L_Com, IP_VHAND3_L);

			connect_status = ConnectVHand30(&datagloves[ID_VHAND_30_LEFT_USB], CONN_USB, STREAM_FINGERS_QUATERNION, false);

			if (connect_status != GLOVE_ADD_SUCCESS)
			{
				connect_status = VHand30_ScanCOM(&datagloves[ID_VHAND_30_LEFT_USB], IP_VHAND3_L);
				if (connect_status != GLOVE_ADD_FAIL)
				{
					VHand3_L_Com = connect_status;
				}
			}
		}

		if (connect_status != GLOVE_ADD_FAIL)
		{
			sensor_id[sensor_number] = ID_VHAND_30_LEFT_USB;
			sensor_number++;
			TCHAR szBuf[512];
			HRESULT hr = StringCchPrintf(szBuf, 512, TEXT("%d"), VHand3_L_Com);
			WriteProfileString(TEXT("cnel"), TEXT("COM_VHAND_30_LEFT"), szBuf);
		}

		return connect_status;
		break;
	case ID_VHAND_30_RIGHT_USB:
		if (VHand3_R_Com == 0)
		{
			//Auto scan the port from 1-50
			connect_status = VHand30_ScanCOM(&datagloves[ID_VHAND_30_RIGHT_USB], IP_VHAND3_L);
			if (connect_status != GLOVE_ADD_FAIL)
			{
				VHand3_R_Com = connect_status;
			}
		}
		else
		{
			if (isSensorAdded(ID_VHAND_30_RIGHT_USB))
			{
				MessageBox(NULL, L"Data glove already added !", L"Warning", MB_OK | MB_ICONWARNING);
				return 0;
			}

			datagloves[ID_VHAND_30_RIGHT_USB].SetConnectionParameters(VHand3_R_Com, IP_VHAND3_R);

			connect_status = ConnectVHand30(&datagloves[ID_VHAND_30_RIGHT_USB], CONN_USB, STREAM_FINGERS_QUATERNION, false);

			if (connect_status != GLOVE_ADD_SUCCESS)
			{
				connect_status = VHand30_ScanCOM(&datagloves[ID_VHAND_30_RIGHT_USB], IP_VHAND3_R);
				if (connect_status != GLOVE_ADD_FAIL)
				{
					VHand3_R_Com = connect_status;
				}
			}
		}

		if (connect_status != GLOVE_ADD_FAIL)
		{
			sensor_id[sensor_number] = ID_VHAND_30_RIGHT_USB;
			sensor_number++;
			TCHAR szBuf[512];
			HRESULT hr = StringCchPrintf(szBuf, 512, TEXT("%d"), VHand3_R_Com);
			WriteProfileString(TEXT("cnel"), TEXT("COM_VHAND_30_RIGHT"), szBuf);
		}
		return connect_status;
		break;
	case ID_VHAND_20_LEFT:
		//to be implemented
		if (isSensorAdded(ID_VHAND_20_LEFT))

		{
			MessageBox(NULL, L"Data glove already added !", L"Warning", MB_OK | MB_ICONWARNING);
			return GLOVE_ADD_FAIL;
		}

		VHand2_L_ID = project->AddDataGlove("Glove", COM_VHAND_20_LEFT, 1);

		if (VHand2_L_ID == -1)
		{
			MessageBox(NULL, L"Cannot add the data glove !", L"Error", MB_OK | MB_ICONERROR);
			return GLOVE_ADD_FAIL;
		}
		//need to change the ID of VHand2.0 to 2
		if (project->StartSampling(VHand2_L_ID) == -1)
		{
			MessageBox(NULL, L"Add data glove success, start sampling!", L"Success", MB_OK | MB_ICONINFORMATION);
		}
		else
		{
			MessageBox(NULL, L"Can not find the glove!", L"Error", MB_OK | MB_ICONERROR);
		}
		sensor_id[sensor_number] = ID_VHAND_20_LEFT;
		sensor_number++;
		return GLOVE_ADD_SUCCESS;
		break;
	case ID_VHAND_20_RIGHT:
		if (isSensorAdded(ID_VHAND_20_RIGHT))

		{
			MessageBox(NULL, L"Data glove already added !", L"Warning", MB_OK | MB_ICONWARNING);
			return GLOVE_ADD_FAIL;
		}

		VHand2_R_ID = project->AddDataGlove("Glove", COM_VHAND_20_RIGHT, 1);

		if (VHand2_R_ID == -1)
		{
			MessageBox(NULL, L"Cannot add the data glove !", L"Error", MB_OK | MB_ICONERROR);
			return GLOVE_ADD_FAIL;
		}
		//need to change the ID of VHand2.0 to 2
		if (project->StartSampling(VHand2_R_ID) == 1)
		{
			MessageBox(NULL, L"Add data glove success, start sampling!", L"Success", MB_OK | MB_ICONINFORMATION);
		}
		else
		{
			MessageBox(NULL, L"Can not find the glove!", L"Error", MB_OK | MB_ICONERROR);
		}
		sensor_id[sensor_number] = ID_VHAND_20_RIGHT;
		sensor_number++;
		return GLOVE_ADD_SUCCESS;
		break;
	default:
		return 0;
	}
}

//index is the index of the data array of the glove 
double Sensor::dataFromGlove(int id,int index) {

	switch (id)
	{
	case ID_VHAND_30_LEFT:
		switch (index)
		{
		case F1:
			return datagloves[ID_VHAND_30_LEFT].Fingers[F1 - 1];
		case F2:
			return datagloves[ID_VHAND_30_LEFT].Fingers[F2 - 1];
		case F3:
			return datagloves[ID_VHAND_30_LEFT].Fingers[F3 - 1];
		case F4:
			return datagloves[ID_VHAND_30_LEFT].Fingers[F4 - 1];
		case F5:
			return datagloves[ID_VHAND_30_LEFT].Fingers[F5 - 1];
		case ROLL:
			return datagloves[ID_VHAND_30_LEFT].Roll;
		case PITCH:
			return datagloves[ID_VHAND_30_LEFT].Pitch;
		case YAW:
			return datagloves[ID_VHAND_30_LEFT].Yaw;
		case AX:
			return datagloves[ID_VHAND_30_LEFT].accel[AX - AX];
		case AY:
			return datagloves[ID_VHAND_30_LEFT].accel[AY - AX];
		case AZ:
			return datagloves[ID_VHAND_30_LEFT].accel[AZ - AX];

		}
		break;
	case ID_VHAND_30_LEFT_USB:
		switch (index)
		{
		case F1:
			return datagloves[ID_VHAND_30_LEFT_USB].Fingers[F1 - 1];
		case F2:
			return datagloves[ID_VHAND_30_LEFT_USB].Fingers[F2 - 1];
		case F3:
			return datagloves[ID_VHAND_30_LEFT_USB].Fingers[F3 - 1];
		case F4:
			return datagloves[ID_VHAND_30_LEFT_USB].Fingers[F4 - 1];
		case F5:
			return datagloves[ID_VHAND_30_LEFT_USB].Fingers[F5 - 1];
		case ROLL:
			return datagloves[ID_VHAND_30_LEFT_USB].Roll;
		case PITCH:
			return datagloves[ID_VHAND_30_LEFT_USB].Pitch;
		case YAW:
			return datagloves[ID_VHAND_30_LEFT_USB].Yaw;
		case AX:
			return datagloves[ID_VHAND_30_LEFT_USB].accel[AX - AX];
		case AY:
			return datagloves[ID_VHAND_30_LEFT_USB].accel[AY - AX];
		case AZ:
			return datagloves[ID_VHAND_30_LEFT_USB].accel[AZ - AX];

		}
		break;

	case ID_VHAND_30_RIGHT:
		switch (index)
		{
		case F1:
			return datagloves[ID_VHAND_30_RIGHT].Fingers[F1 - 1];
		case F2:
			return datagloves[ID_VHAND_30_RIGHT].Fingers[F2 - 1];
		case F3:
			return datagloves[ID_VHAND_30_RIGHT].Fingers[F3 - 1];
		case F4:
			return datagloves[ID_VHAND_30_RIGHT].Fingers[F4 - 1];
		case F5:
			return datagloves[ID_VHAND_30_RIGHT].Fingers[F5 - 1];
		case ROLL:
			return datagloves[ID_VHAND_30_RIGHT].Roll;
		case PITCH:
			return datagloves[ID_VHAND_30_RIGHT].Pitch;
		case YAW:
			return datagloves[ID_VHAND_30_RIGHT].Yaw;
		case AX:
			return datagloves[ID_VHAND_30_RIGHT].accel[AX - AX];
		case AY:
			return datagloves[ID_VHAND_30_RIGHT].accel[AY - AX];
		case AZ:
			return datagloves[ID_VHAND_30_RIGHT].accel[AZ - AX];

		}
		break;
	case ID_VHAND_30_RIGHT_USB:
		switch (index)
		{
		case F1:
			return datagloves[ID_VHAND_30_RIGHT_USB].Fingers[F1 - 1];
		case F2:
			return datagloves[ID_VHAND_30_RIGHT_USB].Fingers[F2 - 1];
		case F3:
			return datagloves[ID_VHAND_30_RIGHT_USB].Fingers[F3 - 1];
		case F4:
			return datagloves[ID_VHAND_30_RIGHT_USB].Fingers[F4 - 1];
		case F5:
			return datagloves[ID_VHAND_30_RIGHT_USB].Fingers[F5 - 1];
		case ROLL:
			return datagloves[ID_VHAND_30_RIGHT_USB].Roll;
		case PITCH:
			return datagloves[ID_VHAND_30_RIGHT_USB].Pitch;
		case YAW:
			return datagloves[ID_VHAND_30_RIGHT_USB].Yaw;
		case AX:
			return datagloves[ID_VHAND_30_RIGHT_USB].accel[AX - AX];
		case AY:
			return datagloves[ID_VHAND_30_RIGHT_USB].accel[AY - AX];
		case AZ:
			return datagloves[ID_VHAND_30_RIGHT_USB].accel[AZ - AX];

		}
		break;
	case ID_VHAND_20_LEFT:

		switch (index)
		{
		case F1:
			return project->GetDataGlove(VHand2_L_ID)->f[index - 1];
		case F2:
			return project->GetDataGlove(VHand2_L_ID)->f[index - 1];
		case F3:
			return project->GetDataGlove(VHand2_L_ID)->f[index - 1];
		case F4:
			return project->GetDataGlove(VHand2_L_ID)->f[index - 1];
		case F5:
			return project->GetDataGlove(VHand2_L_ID)->f[index - 1];
		case ROLL:
			return project->GetDataGlove(VHand2_L_ID)->roll;
		case PITCH:
			return project->GetDataGlove(VHand2_L_ID)->pitch;
		case AX:
			return project->GetDataGlove(VHand2_L_ID)->ax;
		case AY:
			return project->GetDataGlove(VHand2_L_ID)->ay;
		case AZ:
			return project->GetDataGlove(VHand2_L_ID)->az;
		}
		break;
	case ID_VHAND_20_RIGHT:

		switch (index)
		{
		case F1:
			return project->GetDataGlove(VHand2_R_ID)->f[index - 1];
		case F2:
			return project->GetDataGlove(VHand2_R_ID)->f[index - 1];
		case F3:
			return project->GetDataGlove(VHand2_R_ID)->f[index - 1];
		case F4:
			return project->GetDataGlove(VHand2_R_ID)->f[index - 1];
		case F5:
			return project->GetDataGlove(VHand2_R_ID)->f[index - 1];
		case ROLL:
			return project->GetDataGlove(VHand2_R_ID)->roll;
		case PITCH:
			return project->GetDataGlove(VHand2_R_ID)->pitch;
		case AX:
			return project->GetDataGlove(VHand2_R_ID)->ax;
		case AY:
			return project->GetDataGlove(VHand2_R_ID)->ay;
		case AZ:
			return project->GetDataGlove(VHand2_R_ID)->az;
		}
	default:
		return 0;
	}
}

void Sensor::addJoystick() {

}

void Sensor::disconnectGlove()
{

	for (int i = 0; i < sensor_number; ++i)
	{
		disconnectGlove(sensor_id[i]);
	}

	sensor_number = 0;

	MessageBox(NULL, L"All data gloves disconnected !", L"Warning", MB_OK | MB_ICONWARNING);
}
void Sensor::disconnectGlove(int id) {
	switch (id)
	{
	case ID_VHAND_30_LEFT:
		datagloves[ID_VHAND_30_LEFT].Disconnect();
		break;
	case ID_VHAND_30_LEFT_USB:
		datagloves[ID_VHAND_30_LEFT_USB].Disconnect();
		break;
	case ID_VHAND_30_RIGHT:
		datagloves[ID_VHAND_30_RIGHT].Disconnect();
		break;
	case ID_VHAND_30_RIGHT_USB:
		datagloves[ID_VHAND_30_RIGHT_USB].Disconnect();
		break;
	case ID_VHAND_20_LEFT:
		project->StopSampling(VHand2_L_ID);
		project->RemoveGlove(VHand2_L_ID);
		break;
	case ID_VHAND_20_RIGHT:
		project->StopSampling(VHand2_R_ID);
		project->RemoveGlove(VHand2_R_ID);
	}
	
}

void Sensor::turnoffGlove()
{
	int prev = sensor_number;
	/*disconnectGlove();*/
	for (int i = 0; i<prev; ++i)
	{
		turnoffGlove(sensor_id[i]);
	}

	sensor_number = 0;
	MessageBox(NULL, L"All data gloves turned off !", L"Warning", MB_OK | MB_ICONWARNING);
}
void Sensor :: turnoffGlove(int id)
{
	switch (id)
	{
	case ID_VHAND_30_LEFT:
		datagloves[ID_VHAND_30_LEFT].TurnOFF(CONN_WIFI);
		break;
	case ID_VHAND_30_RIGHT:
		datagloves[ID_VHAND_30_RIGHT].TurnOFF(CONN_WIFI);
		break;
	case ID_VHAND_30_LEFT_USB:
		datagloves[ID_VHAND_30_LEFT_USB].TurnOFF(CONN_USB);
		break;
	case ID_VHAND_30_RIGHT_USB:
		datagloves[ID_VHAND_30_RIGHT_USB].TurnOFF(CONN_USB);
		break;
	case ID_VHAND_20_LEFT:
		project->CloseProject();
		break;
	case ID_VHAND_20_RIGHT:
		project->CloseProject();
		break;
	}
}
