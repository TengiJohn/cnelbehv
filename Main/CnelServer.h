#ifndef CnelServer_H
#define CnelServer_H
#include "stdafx.h"

using namespace std;

class CnelServer {
public:
	//for udp&tcp
	
	SOCKET SendSocket;

	//for tcp
	SOCKET ListenSocket;

	sockaddr_in RecvAddr;

	unsigned short Port;

	bool tcp_connected;
	bool udp_connected;

	fd_set read_fds;
	timeval tv;

	//Com
	string com_teensy_usb;
	string com_teensy_bluetooth;
	float com_dynamic_period;
	float com_default_period;
	float com_offset;
	HANDLE com_fd;
	LARGE_INTEGER com_current, com_start, com_fs;

	char* Receive_Buffer;
	int packet_size;

	char* com_buffer;
	int com_ix;
	BOOL buffer_full;
	int packet;
	int buffer_packet;
	double time_difference;
	double* pre_data;

	int com_buff_size;
	int recv_buff_size;
	int pre_data_size;
	
public:
	CnelServer()
	{
		com_buff_size = 1024;
		recv_buff_size = 1024;
		pre_data_size = 100;
		com_buffer = new char[com_buff_size];
		Receive_Buffer = new char[recv_buff_size];
		pre_data = new double[pre_data_size];
	}
	~CnelServer()
	{
		delete[] com_buffer;
		delete[] Receive_Buffer;
		delete[] pre_data;
	}

	void clearBuffer();

	int openUDP(string IPaddress, int port);
	int openTCP(string IPaddress, int port);

	int closeSocket();

	int sendUDP(float* dataptr, int len);
	int sendUDP(double* dataptr, int len);

	int sendTCP(float* dataptr, int len);
	int sendTCP(double* dataptr, int len);

	int openCOM(int id);
	int closeCOM();
	int parseCOMData();
	int addCOMData(double* dataptr, int& data_len);
};

#endif