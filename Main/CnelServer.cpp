#include "CnelServer.h"
void CnelServer::clearBuffer()
{
	for (int i = 0; i < pre_data_size; ++i)
	{
		pre_data[i] = 0;
	}
	for (int i = 0; i < com_buff_size; ++i)
	{
		com_buffer[i] = 0;
	}
	for (int i = 0; i < recv_buff_size; ++i)
	{
		Receive_Buffer[i] = 0;
	}
	buffer_full = false;

	com_ix = 0;
	com_fd = NULL;

	tcp_connected = false;
	udp_connected = false;
}
int CnelServer::closeSocket()
{

	//if (shutdown(SendSocket, SD_SEND) == SOCKET_ERROR)
	//{
	//	wprintf(L"shutdown failed: %d\n", WSAGetLastError());
	//	closesocket(SendSocket);
	//	WSACleanup();
	//	return 1;
	//}
	//---------------------------------------------
	// When the application is finished sending, close the socket.
	wprintf(L"Finished sending. Closing socket.\n");
	int iResult = closesocket(SendSocket);
	if (iResult == SOCKET_ERROR) {
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Clean up and quit.
	wprintf(L"Exiting.\n");
	WSACleanup();

	tcp_connected = false;
	udp_connected = false;
	return 0;
}


int CnelServer::openTCP(string IPaddress, int port)
{
	return 1;
}
int CnelServer::openUDP(string IPaddress, int port)
{
	udp_connected = false;
	this->closeSocket();
	SendSocket = INVALID_SOCKET;

	int iResult;
	WSADATA wsaData;

	//----------------------
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		TCHAR szBuf[512];
		HRESULT hr = StringCchPrintf(szBuf, 512, L"WSAStartup failed with error: %d\n", iResult);
		MessageBox(NULL, szBuf, L"Error", MB_OK | MB_ICONERROR);
		return 1;
	}

	//---------------------------------------------
	// Create a socket for sending data
	SendSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (SendSocket == INVALID_SOCKET) {
		TCHAR szBuf[512];
		HRESULT hr = StringCchPrintf(szBuf, 512, L"socket failed with error: %ld\n", WSAGetLastError());
		MessageBox(NULL, szBuf, L"Error", MB_OK | MB_ICONERROR);
		closesocket(SendSocket);
		WSACleanup();
		return 1;
	}
	//---------------------------------------------
	// Set up the RecvAddr structure with the IP address of
	// the receiver (in this example case "192.168.1.1")
	// and the specified port number.
	string str = IPaddress;
	const char * c = str.c_str();
	RecvAddr.sin_family = AF_INET;
	RecvAddr.sin_port = htons(port);
	RecvAddr.sin_addr.s_addr = inet_addr(c);

	//do you need binding the address ?
	//
	udp_connected = true;
	//MessageBox(NULL, L"UDP connected !", L"Success", MB_OK | MB_ICONINFORMATION);

	return 0;
}

int CnelServer::sendTCP(double* dataptr, int len)
{
	// Send a datagram to the receiver
	char const * p = reinterpret_cast<char const *>(dataptr);

	//wprintf(L"Sending a datagram to the receiver...\n");
	int iResult = send(SendSocket, p, len*sizeof(double), 0);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
		closesocket(SendSocket);
		tcp_connected = false;
		WSACleanup();
		return 1;
	}

	return 0;

}


int CnelServer::sendTCP(float* dataptr, int len)
{
	// Send a datagram to the receiver
	char const * p = reinterpret_cast<char const *>(dataptr);

	//wprintf(L"Sending a datagram to the receiver...\n");
	int iResult = send(SendSocket, p, len*sizeof(float), 0);
	if (iResult == SOCKET_ERROR) {
		wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
		closesocket(SendSocket);
		tcp_connected = false;
		WSACleanup();
		return 1;
	}

	return 0;

}
int CnelServer::sendUDP(float* dataptr, int len) {

	// Send a datagram to the receiver
	char const * p = reinterpret_cast<char const *>(dataptr);

	//wprintf(L"Sending a datagram to the receiver...\n");
	int iResult = sendto(SendSocket, p, len*sizeof(float), 0, (SOCKADDR*)& RecvAddr, sizeof(RecvAddr));
	if (iResult == SOCKET_ERROR) {
		wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
		closesocket(SendSocket);
		udp_connected = false;
		WSACleanup();
		return 1;
	}

	return 0;
}

int CnelServer::sendUDP(double* dataptr, int len) {

	// Send a datagram to the receive
	char const * p = reinterpret_cast<char const *>(dataptr);

	//wprintf(L"Sending a datagram to the receiver...\n");
	int iResult = sendto(SendSocket, p, len*sizeof(double), 0, (SOCKADDR*)& RecvAddr, sizeof(RecvAddr));
	if (iResult == SOCKET_ERROR) {
		wprintf(L"sendto failed with error: %d\n", WSAGetLastError());
		closesocket(SendSocket);
		udp_connected = false;
		WSACleanup();
		return 1;
	}

	return 0;
}

int CnelServer::openCOM(int id)
{
	COMMCONFIG cfg;
	COMMTIMEOUTS timeout;
	DWORD n = 0;
	char portname[256];
	int num;

	string com_teensy;
	switch (id)
	{
	case ID_CONNECT_USB:
		com_teensy = com_teensy_usb;
		break;
	case ID_CONNECT_BLUETOOTH:
		com_teensy = com_teensy_bluetooth;
	}
				
	if (sscanf_s(com_teensy.c_str(), "COM%d", &num) == 1) {
		sprintf_s(portname, "\\\\.\\COM%d", num); // Microsoft KB115831
	}
	else {
		strncpy_s(portname, com_teensy.c_str(), sizeof(portname) - 1);
		portname[n - 1] = 0;
	}

	com_fd = CreateFileA(portname,
		GENERIC_READ | GENERIC_WRITE,
		0,
		0,
		OPEN_EXISTING,
		NULL,
		0);

	//if (fd == INVALID_HANDLE_VALUE) die("unable to open port %s\n", name);

	GetCommConfig(com_fd, &cfg, &n);
	cfg.dcb.BaudRate = 115200;
	cfg.dcb.fBinary = TRUE;
	cfg.dcb.fParity = FALSE;
	cfg.dcb.fOutxCtsFlow = FALSE;
	cfg.dcb.fOutxDsrFlow = FALSE;
	cfg.dcb.fOutX = FALSE;
	cfg.dcb.fInX = FALSE;
	cfg.dcb.fErrorChar = FALSE;
	cfg.dcb.fNull = FALSE;
	cfg.dcb.fRtsControl = RTS_CONTROL_ENABLE;
	cfg.dcb.fAbortOnError = FALSE;
	cfg.dcb.ByteSize = 8;
	cfg.dcb.Parity = NOPARITY;
	cfg.dcb.StopBits = ONESTOPBIT;
	cfg.dcb.fDtrControl = DTR_CONTROL_ENABLE;
	SetCommConfig(com_fd, &cfg, n);
	GetCommTimeouts(com_fd, &timeout);
	timeout.ReadIntervalTimeout = MAXWORD;
	timeout.ReadTotalTimeoutMultiplier = 0;
	timeout.ReadTotalTimeoutConstant = 0;
	timeout.WriteTotalTimeoutConstant = 0;
	timeout.WriteTotalTimeoutMultiplier = 0;
	SetCommTimeouts(com_fd, &timeout);

	return 1;
}

int CnelServer::parseCOMData()
{
	static int k = 0;
	DWORD dwErrors;
	COMSTAT comStat;
	char ch[1];
	DWORD n = 0;
	BOOL r;

	// Calculates time difference between sent packets
	QueryPerformanceCounter(&com_current);
	time_difference = ((double)(com_current.QuadPart - com_start.QuadPart)) / com_fs.QuadPart;
	time_difference *= 1000;
	// Get and clear current errors on the port.
	ClearCommError(com_fd, &dwErrors, &comStat);
	// Reads and parses serial data. Then puts it into a buffer. Once the buffer
	// fills up with the trigger value amount of packets, it enables UDP tranmission
	// mode.
	while (comStat.cbInQue >= 1) {  // comStat.cbInQue bytes have been received, but not read
		r = ReadFile(com_fd,
			ch,
			1,
			&n,
			NULL);

		ClearCommError(com_fd, &dwErrors, &comStat); // Get and clear current errors on the port

		Receive_Buffer[k] = ch[0];
		k++;

		if (ch[0] == '!' && k == packet_size) {
			k = 0;
			if (com_ix <= com_buff_size){
				for (int i = 0; i < packet_size; ++i){
					com_buffer[com_ix] = Receive_Buffer[i];
					com_ix++;
				}
				packet++;
			}
			else{
				printf("buffer overrun\n");
			}

			if (buffer_full == FALSE && packet == buffer_packet) {
				printf("entering UDP mode...\n\n");
				buffer_full = TRUE;
			}
			
		}
		else if (ch[0] == '!' && k != packet_size) {
			printf("\nmisaligned packet\n");
			k = 0;
		}
	}
	return 1;
}

int CnelServer::addCOMData(double* dataptr, int& data_len)
{
	//buffer sending the data
	if (buffer_full && packet > 0 && (buffer_packet==1||time_difference >= com_dynamic_period)) {

		char* tmp_data = new char[packet_size];

		memcpy(tmp_data, com_buffer, sizeof(char)*packet_size);

		sscanf_s(tmp_data, "%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf,%lf!",
			dataptr+data_len, dataptr+data_len + 1, dataptr+data_len + 2, dataptr+data_len + 3, 
			dataptr + data_len + 4, dataptr + data_len + 5, dataptr + data_len + 6, dataptr + data_len + 7,
			dataptr + data_len + 8,dataptr+data_len+9);

		memcpy(pre_data, dataptr+data_len, sizeof(double) * 10);

		--packet;

		//------------------------------------------------------------------
		// Adjusts the timer to send data to UDP by a given offset to keep
		// the buffer level steady.
		if (packet > buffer_packet + 5){
			com_dynamic_period = com_default_period-com_offset;
		}
		else if (packet < buffer_packet - 5){
			com_dynamic_period = com_default_period+com_offset;
		}
		else{
			com_dynamic_period = com_default_period;
		}

		com_start = com_current;//resetting time

		com_ix = com_ix - packet_size;

		printf("Time Difference: %.4lg\tBuffer Level: %4d\r", time_difference, com_ix);

		//flush out the sended packet
		for (int i = 0; i < com_buff_size - packet_size; i++){
			com_buffer[i] = com_buffer[i + packet_size];
		}
		delete[] tmp_data;
	}
	else
	{
		memcpy(dataptr + data_len, pre_data, sizeof(double) * 10);
	}
	data_len += 10;

	return 1;
}

int CnelServer::closeCOM()
{
	bool success = true;
	__try
	{
		success=CloseHandle(com_fd);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		printf("CloseHandle Error\r");
	}
	return 1;
}
