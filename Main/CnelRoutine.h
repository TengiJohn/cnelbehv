#ifndef _CNEL_ROUTINE_H_
#define _CNEL_ROUTINE_H_

#include "stdafx.h"

//Other classes:
#include "Cnel.h"

#include "AVCap.h"

using namespace std;
using namespace avcap;

extern Cnel* cnel;

extern AVCap gAVCap;

void CALLBACK SynchSender(UINT nIDEvent, 
						  UINT msg, 
						  DWORD_PTR dwUser, 
						  DWORD_PTR dw1, 
						  DWORD_PTR dw2);

void CALLBACK CnelReceiver(UINT nIDEvent,
	UINT msg,
	DWORD_PTR dwUser,
	DWORD_PTR dw1,
	DWORD_PTR dw2);

VOID CALLBACK CnelServerRoutine(PVOID lpParam, BOOLEAN TimerOrWaitFired);


#endif