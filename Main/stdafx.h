// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#ifndef _STDAFX_H_
#define _STDAFX_H_

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <iostream>
#include <SDKDDKVer.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <conio.h>
#include <vector>
#include <winsock.h>
#include "wchar.h"
#include "stdio.h"
#include <string>
#include <vector>
#include "tchar.h"
#include "strsafe.h"
#include "streams.h"
#include "resource.h"
#include <math.h>

extern HWND Main_hWnd;
extern void socket_update(int ID_MENU);

#define PI 3.14159265


#endif

// TODO: reference additional headers your program requires here
