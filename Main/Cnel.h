// CNEL wrapper class
#ifndef CNEL_H
#define CNEL_H
#define _CRT_SECURE_NO_WARNINGS
#include "stdafx.h"
//Other classes:
#include "Sensor.h"

#include "CnelServer.h"
//com 

//default timer resolution period 5 ms
#define DEFAULT_TIMER_PERIOD 10

#define OUTPUT_TO_TEXT 0
#define DEFAULT_FILE_NAME "timer_test.txt"

//UDP remote commands
#define START_VIDEO_CAPTURE 1
#define START_VIDEO_UDP 2
#define STOP_VIDEO_CAPTURE 3
#define STOP_VIDEO_UDP 4
#define START_VIDEO_PREVIEW 5
#define STOP_VIDEO_PREVIEW 6
#define CAPTURE_AUDIO 7
#define NOT_CAPTURE_AUDIO 8
#define SOCKET_DISCONNECT 100
#define SOCKET_UDP 101
#define SOCKET_TCP 102

#define UI_TIMER_RES 200

//Com to UDP
#define _CRT_SECURE_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define PORTTYPE HANDLE

//42 for teensy
#define PACKET_SIZE_IN_BYTES 50
#define TEENSY_DATA_LENGTH 10
/*In the format of
A/D,A/D,A/D,A/D,A/D,A/D,A/D,A/D,A/D,A/D!
A/D: 4 bytes
*/

//Defaut sending com port data to udp in ms
#define COM2UDP_OFFSET 1
#define BUFFER_PACKETS 1
class Cnel {
public:
	Sensor* sensor;
	string filename;

	int host_port_remote;

	int host_port_local;

	int timer_period_ms;
	
	int conn_status;

	char host_ip_remote[100];

	sockaddr_in ServerAddr;

	SOCKET ServerSocket;

	fd_set read_fds;
	timeval tv;

	char recv_buffer[50];

	HANDLE hTimerQueue;
	HANDLE hTimer;

	UINT m_nTimerId;
	UINT wTimerRes;

	CnelServer* server;

	FILE * pFile;

	LARGE_INTEGER current_time,start_time,time_fs;
	//Com
public:
	Cnel();

	void setFilename(string file);
	string getFilename();

	void StartServer();

	int Init();
	int Clear();

	double getCurrentTime();
	double getElapsedTime();
	int getTimerFrequency();

};

#endif // !CNEL_H

