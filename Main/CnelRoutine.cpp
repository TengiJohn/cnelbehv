#include"CnelRoutine.h"

#pragma comment(lib, "Ws2_32.lib")
//#pragma comment(lib, "winmm.lib")

void CALLBACK CnelReceiver(UINT nIDEvent, UINT msg, DWORD_PTR dwUser, DWORD dw1, DWORD dw2)
{
}
//Dynamic decide on which data to send
//Video or Video+Glove or Glove ?
//Sequence of priority 
/*1--Video
  2--VHand3.0 Left
  3--VHand3.0 Right
  4--VHand2.0 Left
  5--VHand2.0 Right
  */
void CALLBACK SynchSender(UINT nIDEvent, UINT msg, DWORD_PTR dwUser, DWORD dw1, DWORD dw2)
{
	static int count = 0;
	++count;
	count = count > 1000 ? 1 : count;

	if (cnel->conn_status == SOCKET_DISCONNECT)
		return;
	//QueryPerformanceCounter(&current);
	double videoframe = 0;
	double glove_data[50];

	int data_len = 0;


	if (cnel->sensor->isGloveConnected(ID_VHAND_30_LEFT))
	{
		cnel->sensor->datagloves[ID_VHAND_30_LEFT].GetFingers(glove_data + data_len);
		glove_data[data_len + 5] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT, AX);
		glove_data[data_len + 6] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT, AY);
		glove_data[data_len + 7] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT, AZ);
		glove_data[data_len + 8] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT, ROLL);
		glove_data[data_len + 9] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT, PITCH);
		glove_data[data_len + 10] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT, YAW);

		data_len += 11;
	}
	if (cnel->sensor->isGloveConnected(ID_VHAND_30_LEFT_USB))
	{
		cnel->sensor->datagloves[ID_VHAND_30_LEFT_USB].GetFingers(glove_data + data_len);
		glove_data[data_len + 5] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT_USB, AX);
		glove_data[data_len + 6] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT_USB, AY);
		glove_data[data_len + 7] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT_USB, AZ);
		glove_data[data_len + 8] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT_USB, ROLL);
		glove_data[data_len + 9] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT_USB, PITCH);
		glove_data[data_len + 10] = cnel->sensor->dataFromGlove(ID_VHAND_30_LEFT_USB, YAW);

		data_len += 11;
	}

	if (cnel->sensor->isGloveConnected(ID_VHAND_30_RIGHT))
	{
		cnel->sensor->datagloves[ID_VHAND_20_RIGHT].GetFingers(glove_data + data_len);
		glove_data[data_len + 5] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT, AX);
		glove_data[data_len + 6] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT, AY);
		glove_data[data_len + 7] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT, AZ);
		glove_data[data_len + 8] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT, ROLL);
		glove_data[data_len + 9] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT, PITCH);
		glove_data[data_len + 10] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT, YAW);

		data_len += 11;

	}

	if (cnel->sensor->isGloveConnected(ID_VHAND_30_RIGHT_USB))
	{
		cnel->sensor->datagloves[ID_VHAND_30_RIGHT_USB].GetFingers(glove_data + data_len);
		glove_data[data_len + 5] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT_USB, AX);
		glove_data[data_len + 6] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT_USB, AY);
		glove_data[data_len + 7] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT_USB, AZ);
		glove_data[data_len + 8] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT_USB, ROLL);
		glove_data[data_len + 9] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT_USB, PITCH);
		glove_data[data_len + 10] = cnel->sensor->dataFromGlove(ID_VHAND_30_RIGHT_USB, YAW);

		data_len += 11;

	}

	if (cnel->sensor->isGloveConnected(ID_VHAND_20_LEFT))
	{
		glove_data[data_len] = cnel->sensor->dataFromGlove(ID_VHAND_20_LEFT, F1);
		glove_data[data_len + 1] = cnel->sensor->dataFromGlove(ID_VHAND_20_LEFT, F2);
		glove_data[data_len + 2] = cnel->sensor->dataFromGlove(ID_VHAND_20_LEFT, F3);
		glove_data[data_len + 3] = cnel->sensor->dataFromGlove(ID_VHAND_20_LEFT, F4);
		glove_data[data_len + 4] = cnel->sensor->dataFromGlove(ID_VHAND_20_LEFT, F5);
		glove_data[data_len + 5] = cnel->sensor->dataFromGlove(ID_VHAND_20_LEFT, AX);
		glove_data[data_len + 6] = cnel->sensor->dataFromGlove(ID_VHAND_20_LEFT, AY);
		glove_data[data_len + 7] = cnel->sensor->dataFromGlove(ID_VHAND_20_LEFT, AZ);
		glove_data[data_len + 8] = cnel->sensor->dataFromGlove(ID_VHAND_20_LEFT, ROLL);
		glove_data[data_len + 9] = cnel->sensor->dataFromGlove(ID_VHAND_20_LEFT, PITCH);

		data_len += 10;

	}

	if (cnel->sensor->isGloveConnected(ID_VHAND_20_RIGHT))
	{
		glove_data[data_len] = cnel->sensor->dataFromGlove(ID_VHAND_20_RIGHT, F1);
		glove_data[data_len + 1] = cnel->sensor->dataFromGlove(ID_VHAND_20_RIGHT, F2);
		glove_data[data_len + 2] = cnel->sensor->dataFromGlove(ID_VHAND_20_RIGHT, F3);
		glove_data[data_len + 3] = cnel->sensor->dataFromGlove(ID_VHAND_20_RIGHT, F4);
		glove_data[data_len + 4] = cnel->sensor->dataFromGlove(ID_VHAND_20_RIGHT, F5);
		glove_data[data_len + 5] = cnel->sensor->dataFromGlove(ID_VHAND_20_RIGHT, AX);
		glove_data[data_len + 6] = cnel->sensor->dataFromGlove(ID_VHAND_20_RIGHT, AY);
		glove_data[data_len + 7] = cnel->sensor->dataFromGlove(ID_VHAND_20_RIGHT, AZ);
		glove_data[data_len + 8] = cnel->sensor->dataFromGlove(ID_VHAND_20_RIGHT, ROLL);
		glove_data[data_len + 9] = cnel->sensor->dataFromGlove(ID_VHAND_20_RIGHT, PITCH);

		data_len += 10;
	}
	if (gAVCap.isCapturing())
	{
		videoframe = gAVCap.getFrameNum();
		glove_data[data_len] = videoframe;
		data_len++;
	}
	else
	{
		glove_data[data_len] = -1;
		data_len++;
	}
	if (cnel->sensor->isTeensyConnected())
	{
		cnel->server->parseCOMData();
		cnel->server->addCOMData(glove_data, data_len);
		for (int i = data_len - 1; i > data_len - 12; --i)
			glove_data[i] -= 1000;
	}

	//glove_data[data_len] = sin(count*PI*2*4/cnel->getTimerFrequency());
	//glove_data[data_len+1] = (int)2000*sin(count*PI * 2 * 4 / cnel->getTimerFrequency());
	//if (++count > cnel->getTimerFrequency())
	//	count = 0;
	//glove_data[data_len + 2] = cnel->getElapsedTime();
	//data_len += 3;

	//if (video_CnelServer->tcp_connected)
	//{
	//	video_CnelServer->sendTCP(&videoframe, 1);
	//}
	//else if (video_CnelServer->udp_connected)
	//{
	//	video_CnelServer->sendUDP(&videoframe, 1);
	//}

	//teensy
	//**************************************************************************************************************************************
	if (cnel->server->tcp_connected)
	{
		cnel->server->sendTCP(glove_data, data_len);
	}
	else if (cnel->server->udp_connected)
	{
		cnel->server->sendUDP(glove_data, data_len);
	}

	//output to file for test
	if (OUTPUT_TO_TEXT)
	{
		if (data_len > 0)
		{
			for (int i = 0; i < data_len; ++i)
			{
				fprintf(cnel->pFile, "%f,", glove_data[i]);
			}
			fprintf(cnel->pFile, "\n");
		}
	}
}

VOID CALLBACK CnelServerRoutine(PVOID lpParam, BOOLEAN TimerOrWaitFired)
{
	//Clear the receiver buffer
	if (cnel->conn_status == SOCKET_DISCONNECT)
		return;
		
	memset(cnel->recv_buffer, 0, sizeof(cnel->recv_buffer));
	int iResult = recvfrom(cnel->ServerSocket, //A descriptor identifying a bound socket
		cnel->recv_buffer,					//receive buffer
		sizeof(cnel->recv_buffer),			//size of receive buffer
		0,												//flag
		NULL,											//source address (optional)
		NULL											//size of source address (optional)
		);

	if (iResult == SOCKET_ERROR) {
		int ierr = WSAGetLastError();
		if (ierr == WSAEWOULDBLOCK)
		{
			// currently no data available
			//do nothing
		}
		else
		{
			TCHAR szBuf[512];
			closesocket(cnel->ServerSocket);
			cnel->conn_status = SOCKET_DISCONNECT;
			WSACleanup();
			HRESULT hr = StringCchPrintf(szBuf, 512, L"recvfrom failed with error in CnelServerRoutine: %ld\n", ierr);
			MessageBox(NULL, szBuf, L"Error", MB_OK | MB_ICONERROR);
			//exit(0);
		}
	}
	else
	{

		if (strcmp(cnel->recv_buffer, "Start Capture") == 0)
		{
			SendMessage(AMCap::ghwndApp, WM_COMMAND, MENU_START_CAP, 0);
		}
		else if (strcmp(cnel->recv_buffer, "Stop Capture") == 0)
		{
			SendMessage(AMCap::ghwndApp, WM_COMMAND, MENU_STOP_CAP, 0);
		}
		else if (strcmp(cnel->recv_buffer, "Preview") == 0)
		{
			SendMessage(AMCap::ghwndApp, WM_COMMAND, MENU_PREVIEW, 0);
		}
	}
}