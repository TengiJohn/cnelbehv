#ifndef SENSOR_H
#define SENSOR_H

#include <tchar.h>
#include <strsafe.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <ctype.h>
#include <stdio.h>
#include <Windows.h>
#include <string>

#include "Project.h"
#include "VHand30.h"
#define MAX_SENSORS 100

#define GLOVE_ADD_SUCCESS 1
#define GLOVE_ADD_FAIL 0

#define ID_VHAND_30_LEFT 0
#define ID_VHAND_30_RIGHT 1
#define ID_VHAND_30_LEFT_USB 2
#define ID_VHAND_30_RIGHT_USB 3

#define ID_VHAND_20_LEFT 4
#define ID_VHAND_20_RIGHT 5

#define COM_VHAND_20_LEFT 3
#define COM_VHAND_20_RIGHT 3

#define F1 1
#define F2 2
#define F3 3
#define F4 4
#define F5 5
#define ROLL 6
#define PITCH 7
#define YAW 8
#define AX 9
#define AY 10
#define AZ 11

#define LOCAL_HOST "127.0.0.1"


#define HOST_PORT_REMOTE 27002

#define HOST_PORT_LOCAL 27100

//The ip address of the wifi glove needs to be reserved in the router
#define TIME_CONNECT 5 //try 5 seconds to establish wifi connection
#define IP_VHAND3_R "192.168.0.101"
#define IP_VHAND3_L "192.168.0.102"
using namespace std;

class Sensor {
public:

	int max_sensor =100;
	//Sensor ID
	//0-VHand3.0 Left, 1-VHand3.0 Right
	int* sensor_id;
	int sensor_number;
	
	//VHand2.0 dataGloves
	Project* project;

	//VHand3.0 dataGloves
	VHand30* datagloves;
	//Try to connect the dataglove for 5 seconds
	int TryTime = 5 * 1000;

	int VHand2_L_ID;
	int VHand2_R_ID;

	int VHand3_L_Com;
	int VHand3_R_Com;

	int Teensy_USB_Com;
	int Teensy_Bluetooth_Com;

	bool teensy_connected;

public:
	Sensor();
	~Sensor();

	int addGlove(int id);
	int ConnectVHand30(VHand30* dg,int CONN_TYPE,int STREAM_MODE,bool silent);
	int VHand30_ScanCOM(VHand30* dg,char* ip);

	void addJoystick();
	double dataFromGlove(int id,int index);

	void disconnectGlove();
	void disconnectGlove(int id);
	
	void turnoffGlove();
	void turnoffGlove(int id);

	int getChanNum();//return the number of channels of sensor
	float* getChanData();//return the data of sensor 
	string* getChanName();//return the channel name of sensor

	bool isSensorAdded(int id);

	bool isGloveConnected(int id);

	bool isTeensyConnected();
};

#endif