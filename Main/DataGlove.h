#pragma once

class DataGlove
{
public:
	DataGlove();
	~DataGlove(void);

	char label[256];
	int id;
	int comport;
	int type;

	int   fmin[5], fmax[5];	// valori minimo a massimo in raw 0..1024
	float f[5];				// finger 0.0 ... 100.0
	float roll, pitch;		// angolo di rotazione -90.. +90
	float ax, ay, az;		// accelerazione in g -2...+2;

	bool NewDataAvailable;
	bool Sampling;
	bool PortOpened;
	bool SelfCalibration;
	int counter_buffer;
	int buffersize;	// dimensione buffer dei finger
	int buff[2000];
	int buffaccsize; // dimensione buffer delle accelerazioni
	int buffacc[2000];

	int ResetValue(void);
	int ElaborateValues(void);
	int AddPackage(unsigned char * buffer);
};
