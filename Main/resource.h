//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Main.rc
//
#define IDC_MYICON                      2
#define IDD_MAIN_DIALOG                 102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MAIN                        107
#define IDI_SMALL                       108
#define IDC_MAIN                        109
#define IDR_MAINFRAME                   128
#define ID_DEVICES_VIDEO                32771
#define ID_DEVICES_GLOVE                32772
#define ID_CONSOLE_INIT                 32773
#define ID_GLOVE_VHAND3                 32774
#define ID_VHAND3_LEFT                  32775
#define ID_VHAND3_RIGHT                 32776
#define ID_VHAND3_BOTH                  32777
#define ID_GLOVE_VHAND2                 32778
#define ID_LEFT_ADD                     32779
#define ID_LEFT_START                   32780
#define ID_LEFT_TURNOFF                 32781
#define ID_RIGHT_CONNECT                32782
#define ID_RIGHT_SAMPLE                 32783
#define ID_RIGHT_TURNOFF                32784
#define ID_BOTH_CONNECT                 32785
#define ID_BOTH_SAMPLE                  32786
#define ID_BOTH_TURNOFF                 32787
#define ID_DEVICES_GLOVE32788           32788
#define ID_GLOVE_ADD                    32789
#define ID_GLOVE_SAMPLE                 32790
#define ID_GLOVE_TURNOFF                32791
#define ID_ADD_VHAND3                   32792
#define ID_ADD_VHAND2                   32793
#define ID_VHAND3_LEFT32794             32794
#define ID_VHAND3_RIGHT32795            32795
#define ID_GLOVE_DISCONNECT             32796
#define ID_VHAND2_LEFT                  32797
#define ID_VHAND2_RIGHT                 32798
#define ID_CONNECT_VHAND3               32799
#define ID_VHAND3_LEFT32800             32800
#define ID_VHAND3_RIGHT32801            32801
#define ID_VHAND3_USB_LEFT              32802
#define ID_VHAND3_USB_RIGHT             32803
#define ID_SOCKET_TCP                   32804
#define ID_SOCKET_UDP                   32805
#define ID_TCP_LISTEN                   32806
#define ID_SOCKET_DISCONNECT            32807
#define ID_SOCKET_TCP32808              32808
#define ID_TCP_IP                       32809
#define ID_TEENSY_CONNECT               32810
#define ID_TEENSY_DISCONNECT            32811
#define ID_TEENSY_SETTINGS              32812
#define ID_CONNECT_USB                  32813
#define ID_CONNECT_BLUETOOTH            32814
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32815
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
