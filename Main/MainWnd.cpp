#pragma once

#include "MainWnd.h"

HINSTANCE hInst;								// current instance
HINSTANCE hPrev;								// previous instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
AVCap gAVCap;
HWND Main_hWnd;

void socket_update(int ID_MENU);

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MAIN));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MAIN);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, HINSTANCE hPrevInstance,int nCmdShow)
{

   hInst = hInstance; // Store instance handle in our global variable
   hPrev = hPrevInstance;

   Main_hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, MAIN_WIDTH, MAIN_HEIGHT, NULL, NULL, hInstance, NULL);

   if (!Main_hWnd)
   {
      return FALSE;
   }

   ShowWindow(Main_hWnd, nCmdShow);

   UpdateWindow(Main_hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_DEVICES_VIDEO:


			if(!gAVCap.AVCapInit(hInst,hPrev,SW_SHOW))
				return FALSE;
			break;
		case ID_DEVICES_GLOVE:
			break;
		case ID_VHAND3_USB_LEFT:
			cnel->sensor->addGlove(ID_VHAND_30_LEFT_USB);
			break;
		case ID_VHAND3_USB_RIGHT:
			cnel->sensor->addGlove(ID_VHAND_30_RIGHT_USB);
			break;
		case ID_VHAND3_LEFT:
			cnel->sensor->addGlove(ID_VHAND_30_LEFT);
			break;
		case ID_VHAND3_RIGHT:
			cnel->sensor->addGlove(ID_VHAND_30_RIGHT);
			break;
		case ID_VHAND2_LEFT:
			cnel->sensor->addGlove(ID_VHAND_20_LEFT);
			break;
		case ID_VHAND2_RIGHT:
			cnel->sensor->addGlove(ID_VHAND_20_RIGHT);
			break;
		case ID_GLOVE_DISCONNECT:
			cnel->sensor->disconnectGlove();
			break;
		case ID_GLOVE_TURNOFF:
			cnel->sensor->turnoffGlove();
			break;
		case ID_TCP_IP:
			socket_update(ID_TCP_IP);
			break;
		case ID_SOCKET_UDP:
			socket_update(ID_SOCKET_UDP);
			break;
		case ID_SOCKET_DISCONNECT:
			socket_update(ID_SOCKET_DISCONNECT);
			break;
		case ID_CONNECT_USB:
			cnel->server->openCOM(ID_CONNECT_USB);
			cnel->sensor->teensy_connected = true;
			break;
		case ID_CONNECT_BLUETOOTH:
			cnel->server->openCOM(ID_CONNECT_BLUETOOTH);
			cnel->sensor->teensy_connected = true;
			break;
		case ID_TEENSY_DISCONNECT:
			cnel->server->closeCOM();
			cnel->sensor->teensy_connected = false;
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
void socket_update(int ID_MENU)
{
	HMENU hMenu = GetMenu(Main_hWnd);

	switch (ID_MENU)
	{
	case ID_TCP_IP:
		
		//CheckMenuItem(hMenu, ID_TCP_IP, MF_CHECKED);
		//CheckMenuItem(hMenu, ID_SOCKET_UDP, MF_UNCHECKED);
		//CheckMenuItem(hMenu, ID_SOCKET_DISCONNECT, MF_UNCHECKED);

		//video_CnelServer->closeSocket();
		//video_CnelServer->openTCP(cnel->ip, cnel->port_tcp_video);

		//server->closeSocket();
		//server->openTCP(cnel->ip, cnel->port_tcp_glove);

		//cnel->conn_status = SOCKET_TCP;
		break;
	case ID_SOCKET_UDP:
		CheckMenuItem(hMenu, ID_TCP_IP, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SOCKET_UDP, MF_CHECKED);
		CheckMenuItem(hMenu, ID_SOCKET_DISCONNECT, MF_UNCHECKED);

		//video_CnelServer->closeSocket();
		//video_CnelServer->openUDP(cnel->host_ip, cnel->host_port_video);

		cnel->server->closeSocket();
		cnel->server->openUDP(cnel->host_ip_remote, cnel->host_port_remote);

		cnel->conn_status = SOCKET_UDP;
		break;
	case ID_SOCKET_DISCONNECT:
		CheckMenuItem(hMenu, ID_TCP_IP, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SOCKET_UDP, MF_UNCHECKED);
		CheckMenuItem(hMenu, ID_SOCKET_DISCONNECT, MF_CHECKED);

		//video_CnelServer->closeSocket();
		//closesocket(video_CnelServer->ListenSocket);
		//WSACleanup();
		//video_CnelServer->udp_connected = false;
		//video_CnelServer->tcp_connected = false;

		cnel->server->closeSocket();
		closesocket(cnel->server->ListenSocket);
		WSACleanup();
		cnel->server->udp_connected = false;
		cnel->server->tcp_connected = false;

		cnel->conn_status = SOCKET_DISCONNECT;
		//MessageBox(NULL, L"Sockets closed !", L"Warning", MB_OK | MB_ICONWARNING);
		break;
	}
}