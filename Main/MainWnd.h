#pragma once

#include "resource.h"

#include "CnelRoutine.h"

using namespace std;
using namespace avcap;

//defines the size of main window
#define MAIN_WIDTH 500
#define MAIN_HEIGHT 400

#define MAX_LOADSTRING 100
// Global Variables:
extern HINSTANCE hInst;								// current instance
extern TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
extern TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
extern HINSTANCE hPrev;								// previous instance


INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
ATOM				VideoRegisterClass(HINSTANCE hInstance);

BOOL				InitInstance(HINSTANCE, HINSTANCE,int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);

class MainWnd
{
public:

	
};