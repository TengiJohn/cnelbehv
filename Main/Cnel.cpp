#include "Cnel.h"
#include "MainWnd.h"

Cnel::Cnel() {
	sensor = new Sensor();
	server = new CnelServer();
}


void Cnel::setFilename(string str) {
	filename = str;
};

string Cnel::getFilename() {
	return filename;
};

int Cnel::Init()
{
	//Check if there is com port information stored
	this->sensor->VHand3_L_Com = GetProfileInt(TEXT("cnel"), TEXT("COM_VHAND_30_LEFT"), 0);
	this->sensor->VHand3_R_Com = GetProfileInt(TEXT("cnel"), TEXT("COM_VHAND_30_RIGHT"), 0);
	this->sensor->Teensy_USB_Com = GetProfileInt(TEXT("cnel"), TEXT("COM_TEENSY_USB"), 0);
	this->sensor->Teensy_Bluetooth_Com = GetProfileInt(TEXT("cnel"), TEXT("COM_TEENSY_BLUETOOTH"), 0);

	this->host_port_remote = GetProfileInt(TEXT("cnel"), TEXT("HOST_PORT_REMOTE"), HOST_PORT_REMOTE);
	this->host_port_local = GetProfileInt(TEXT("cnel"), TEXT("HOST_PORT_LOCAL"), HOST_PORT_LOCAL);

	this->timer_period_ms = GetProfileInt(TEXT("cnel"), TEXT("TIMER_PERIOD_MS"), DEFAULT_TIMER_PERIOD);

	server->com_default_period = GetProfileInt(TEXT("cnel"), TEXT("UDP_PERIOD_MS"), this->timer_period_ms);

	server->buffer_packet = GetProfileInt(TEXT("cnel"), TEXT("COM_BUFFER_PACKETS"), BUFFER_PACKETS);

	server->clearBuffer();
	
	
    server->com_teensy_usb = "COM" + to_string(sensor->Teensy_USB_Com);
	server->com_teensy_bluetooth = "COM" + to_string(sensor->Teensy_Bluetooth_Com);;

	server->packet_size = PACKET_SIZE_IN_BYTES;

	server->com_dynamic_period = server->com_default_period;
	server->com_offset = COM2UDP_OFFSET;

	TCHAR temp_ip[100];
	GetProfileString(TEXT("cnel"), TEXT("HOST_IP_REMOTE"), TEXT(LOCAL_HOST),
		temp_ip, NUMELMS(temp_ip));

	size_t i;
	wcstombs_s(&i, host_ip_remote, sizeof(host_ip_remote), temp_ip, sizeof(temp_ip));

	HMENU hMenu = GetMenu(Main_hWnd);
	conn_status = GetProfileInt(TEXT("cnel"), TEXT("SOCKET_STATUS"), SOCKET_DISCONNECT);
	switch (conn_status)
	{
	case SOCKET_DISCONNECT:
		socket_update(ID_SOCKET_DISCONNECT);
		break;
	case SOCKET_UDP:
		socket_update(ID_SOCKET_UDP);
		break;
	case SOCKET_TCP:
		socket_update(ID_TCP_IP);
		break;
	}

	//setup server socket on localhost
	StartServer();
	//windows queue timer for cnel server control**************************************************************************
	hTimer = NULL;
	hTimerQueue = NULL;

	// Create the timer queue.
	hTimerQueue = CreateTimerQueue();
	if (NULL == hTimerQueue)
	{
		printf("CreateTimerQueue failed (%d)\n", GetLastError());
		exit(0);
	}

	// Set a timer to call the timer routine in 1 seconds.
	if (!CreateTimerQueueTimer(&hTimer, hTimerQueue,
		(WAITORTIMERCALLBACK)CnelServerRoutine, NULL, 1000, UI_TIMER_RES, 0))
	{
		printf("CreateTimerQueueTimer failed (%d)\n", GetLastError());
		exit(0);
	}
	//********************************************************************************************************

	setFilename(DEFAULT_FILE_NAME);

	//open the data file to write
	if (OUTPUT_TO_TEXT)
	fopen_s(&pFile, DEFAULT_FILE_NAME, "w");

	//multimedia timer for data transmission********************************************************************
	TIMECAPS tc;

	if (timeGetDevCaps(&tc, sizeof(TIMECAPS)) != TIMERR_NOERROR)
		exit(0);
	wTimerRes = min(max(tc.wPeriodMin, 0), tc.wPeriodMax);

	timeBeginPeriod(wTimerRes);

	//uDelay=5 ms
	//uResolution=0 indicates the timer will fire with the greatest accuracy
	m_nTimerId = timeSetEvent(timer_period_ms, wTimerRes, SynchSender, NULL, TIME_PERIODIC);

	QueryPerformanceCounter(&server->com_start);
	QueryPerformanceFrequency(&server->com_fs);

	QueryPerformanceFrequency(&time_fs);
	QueryPerformanceCounter(&start_time);
	return 0;
}


void Cnel::StartServer()
{
	//Clear out server addr
	memset((void *)&ServerAddr, '\0', sizeof(struct sockaddr_in));

	ServerAddr.sin_family = AF_INET;
	ServerAddr.sin_port = htons(host_port_local);
	ServerAddr.sin_addr.s_addr = inet_addr(host_ip_remote);

	//Automatically set the address of current IP
	//char host_name[100];
	//gethostname(host_name, sizeof(host_name));
	//hostent* hp = gethostbyname(host_name);

	//if (hp == NULL)
	//{
	//	MessageBox(NULL, L"Could not get host name.\n", L"Error", MB_OK | MB_ICONERROR);
	//	closesocket(SendSocket);
	//	WSACleanup();
	//	exit(0);
	//}

	//ServerAddr.sin_addr.S_un.S_un_b.s_b1 = hp->h_addr_list[0][0];
	//ServerAddr.sin_addr.S_un.S_un_b.s_b2 = hp->h_addr_list[0][1];
	//ServerAddr.sin_addr.S_un.S_un_b.s_b3 = hp->h_addr_list[0][2];
	//ServerAddr.sin_addr.S_un.S_un_b.s_b4 = hp->h_addr_list[0][3];
	int iResult;
	WSADATA wsaData;

	//----------------------
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		TCHAR szBuf[512];
		HRESULT hr = StringCchPrintf(szBuf, 512, L"WSAStartup failed with error: %d\n", iResult);
		MessageBox(NULL, szBuf, L"Error", MB_OK | MB_ICONERROR);
		exit(0);
	}

	ServerSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (ServerSocket == INVALID_SOCKET) {
		TCHAR szBuf[512];
		HRESULT hr = StringCchPrintf(szBuf, 512, L"socket failed with error: %ld\n", WSAGetLastError());
		MessageBox(NULL, szBuf, L"Error", MB_OK | MB_ICONERROR);
		closesocket(ServerSocket);
		WSACleanup();
		exit(0);
	}
	if (bind(ServerSocket, (struct sockaddr *)&ServerAddr, sizeof(struct sockaddr_in)) == SOCKET_ERROR)
	{
		TCHAR szBuf[512];
		HRESULT hr = StringCchPrintf(szBuf, 512, L"Bind socket with error: %ld\n", WSAGetLastError());
		MessageBox(NULL, szBuf, L"Error", MB_OK | MB_ICONERROR);
		closesocket(ServerSocket);
		WSACleanup();
		exit(0);
	}

	//if iMode!=0 it is non-blocking socket
	u_long iMode = 1;
	int ctn=ioctlsocket(ServerSocket, FIONBIO, &iMode);
	if (ctn == -1)
	{
	}
}

int Cnel::Clear()
{
	//Clean the UI control timer*******************************************************
	// Delete all timers in the timer queue.
	if (!DeleteTimerQueue(hTimerQueue))
		printf("DeleteTimerQueue failed (%d)\n", GetLastError());
	//*********************************************************************************

	if (m_nTimerId)
	{
		timeKillEvent(m_nTimerId);
		timeEndPeriod(wTimerRes);
		m_nTimerId = 0;
	}

	TCHAR szBuf[512];
	HRESULT hr = StringCchPrintf(szBuf, 512, TEXT("%d"), timer_period_ms);
	WriteProfileString(TEXT("cnel"), TEXT("TIMER_PERIOD_MS"), szBuf);

	//video_CnelServer->closeSocket();
	server->closeSocket();
	server->closeCOM();
	//close file
	if (OUTPUT_TO_TEXT)
	fclose(pFile);

	//hr = StringCchPrintf(szBuf, 512, TEXT("%d"), cnel->conn_status);
	hr = StringCchPrintf(szBuf, 512, TEXT("%d"), SOCKET_UDP);
	WriteProfileString(TEXT("cnel"), TEXT("SOCKET_STATUS"), szBuf);

	hr = StringCchPrintf(szBuf, 512, TEXT("%d"), host_port_remote);
	WriteProfileString(TEXT("cnel"), TEXT("HOST_PORT_REMOTE"), szBuf);

	hr = StringCchPrintf(szBuf, 512, TEXT("%d"), host_port_local);
	WriteProfileString(TEXT("cnel"), TEXT("HOST_PORT_LOCAL"), szBuf);

	return 0;
}

double Cnel::getCurrentTime()
{
	QueryPerformanceCounter(&current_time);

	double t = ((double)(current_time.QuadPart - start_time.QuadPart)) / time_fs.QuadPart * 1000;
	return t;
}

double Cnel::getElapsedTime()
{
	LARGE_INTEGER pre_time = current_time;
	QueryPerformanceCounter(&current_time);

	double t = ((double)(current_time.QuadPart - pre_time.QuadPart)) / time_fs.QuadPart * 1000;
	return t;
}

int Cnel::getTimerFrequency()
{
	return 1000 / timer_period_ms;
}