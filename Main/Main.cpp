// Main.cpp : Defines the entry point for the application.
//
#include "MainWnd.h"

Cnel* cnel = new Cnel();

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	//#ifdef _DEBUG
	//RedirectIOToConsole();
	//#endif

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MAIN, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, hPrevInstance,nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MAIN));


	/*Initialize Cnel*/
	cnel->Init();
	// Main message loop
	while(true)
	{
		while (PeekMessage(&msg, NULL, 0, 0,PM_REMOVE))
		{
			if(msg.message==WM_QUIT)
				break;

			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		if (msg.message==WM_QUIT)
			break;

		WaitMessage();
	}

	cnel->Clear();
	// Reached on WM_QUIT message
	CoUninitialize();
	return (int) msg.wParam;
}



