#include"AVCap.h"
#include <iostream>

using namespace avcap;
using namespace std;
/*----------------------------------------------------------------------------*\
|   WinMain( hInst, hPrev, lpszCmdLine, cmdShow )                              |
|                                                                              |
|   Description:                                                               |
|       The main procedure for the App.  After initializing, it just goes      |
|       into a message-processing loop until it gets a WM_QUIT message         |
|       (meaning the app was closed).                                          |
|                                                                              |
|   Arguments:                                                                 |
|       hInst           instance handle of this instance of the app            |
|       hPrev           instance handle of previous instance, NULL if first    |
|       szCmdLine       ->null-terminated command line                         |
|       cmdShow         specifies how the window is initially displayed        |
|                                                                              |
|   Returns:                                                                   |
|       The exit code as specified in the WM_QUIT message.                     |
|                                                                              |
\*----------------------------------------------------------------------------*/
int PASCAL WinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR szCmdLine, int sw)
{
	MSG msg;
	AVCap cap;

	/* Call initialization procedure */
	if(!cap.AVCapInit(hInst,hPrev,sw))
		return FALSE;

	/*
	* Polling messages from event queue
	*/
	for(;;)
	{
		//long fnum=cap.getFrameNum();
		//cout<<fnum<<endl;

		while(PeekMessage(&msg, NULL, 0, 0,PM_REMOVE))
		{
			if(msg.message == WM_QUIT)
				break;  // Leave the PeekMessage while() loop

			if(TranslateAccelerator(cap.getHwnd(), cap.getHAccel(), &msg))
				continue;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if(msg.message == WM_QUIT)
			break;  // Leave the for() loop

		WaitMessage();

	}

	// Reached on WM_QUIT message
	CoUninitialize();

	return ((int) msg.wParam);
}