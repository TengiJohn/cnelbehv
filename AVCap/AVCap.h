#ifndef _AVCAP_H_
#define _AVCAP_H_

#include "stdafxVideo.h"
#include <dbt.h>
#include <mmreg.h>
#include <msacm.h>
#include <fcntl.h>
#include <io.h>
#include <stdio.h>
#include <commdlg.h>
#include "amcap.h"
#include "status.h"

namespace avcap
{
	class AVCap
	{
		long framenum;
	public:
		AVCap()
		{
			framenum=-1;
		}
		~AVCap()
		{}

	public:
		int AVCapInit(HINSTANCE hInst, HINSTANCE hPrev, int sw)
		{return AMCap::AppInit( hInst,  hPrev,  sw);}

		HWND getHwnd()
		{return AMCap::ghwndApp;}

		HACCEL getHAccel()
		{return AMCap::ghAccel;}

		int StartPreview()
		{return AMCap::StartPreview();}

		int StopPreview()
		{return AMCap::StopPreview();}

		int StartCapture()
		{return AMCap::StartCapture();}

		int StopCapture()
		{return StopCapture();}

		long getFrameNum()
		{

			if (!AMCap::gcap)
			{
				framenum=-1;
				return -1;
			}
			else if (!AMCap::gcap->pDF)
			{
				framenum=-1;
				return -1;
			}

			if(AMCap::gcap->pDF->GetNumNotDropped(&framenum)!=S_OK)
			{
				framenum=-1;
				return -1;
			}
			else
			{
				framenum-=AMCap::gcap->lNotBase;
				return framenum;
			}
		}

		BOOL isCapturing()
		{
			return AMCap::gcap->fCapturing;
		}
	};
}
#endif